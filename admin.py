from django.contrib import admin
from .models import ds, fr, dsParameter, frParameter

admin.site.register(ds)
admin.site.register(fr)
admin.site.register(dsParameter)
admin.site.register(frParameter)

# Register your models here.
