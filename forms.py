from django import forms
from .models import fr

class frForm(forms.ModelForm):
    
    class Meta:
        model = fr
        fields = ('name', 'description')