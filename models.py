from django.db import models
import uuid
# Create your models here.
## CHECKUP FOREIGN KEYS on inherited models!!!!!

class boxObject(models.Model):
    ''' master object for all DS, Fr and C '''
    type="object"
    name = models.CharField(max_length=200)
    #shortHand = models.CharField(max_length=100, unique=True, default = "{}-{}".format(type, str(id)) )
    description = models.TextField(blank=True)
    class Meta:
        abstract = True
    
    def __str__(self):
        return self.name
    
class ds(boxObject):
    #isb = models.ForeignKey('fr', on_delete=models.CASCADE, blank=True)
    isb = models.OneToOneField('fr', on_delete=models.CASCADE, null=True)
    image = models.FileField(upload_to='ds_images', null=True, blank=True) 
    type="ds"

    
class fr(boxObject):
    rf = models.ForeignKey('ds', on_delete=models.CASCADE, null=True, blank=True)
    type="fr"
       

class parameter(models.Model):
    ''' 
    Master class for all parameters in an object 
    if possible should also be used in DR elements
    needs to be equipped with evaluation and mathematical functions later!
    '''
    
    object = models.ForeignKey('boxObject', on_delete=models.CASCADE)
    
    name = models.CharField(max_length=200)
    value = models.CharField(max_length=200, blank=True)
    
    def __str__(self):
        return (self.name) 
        
    class Meta:
        abstract = True

class dsParameter(parameter):
    object = models.ForeignKey(ds, on_delete=models.CASCADE)
class frParameter(parameter):
    object = models.ForeignKey(fr, on_delete=models.CASCADE)