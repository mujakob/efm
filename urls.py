from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path, include
from . import views

app_name = "EFM"
urlpatterns = [
    # /efm/ - overview
    path('', views.indexView.as_view(), name='index'),
    path('tree/<int:pk>/', views.efmTreeView.as_view(), name='treeView'),
    path('fr/<int:pk>/', views.frDetail.as_view(), name='frDetail'),
    path('ds/<int:pk>/', views.dsDetail.as_view(), name='dsDetail'),
    path('ds/', views.dsDetail.as_view(), name='dsSlot'),
    path('fr/', views.frDetail.as_view(), name='frSlot'),
    url(r'^ds/(?P<pk>\d+)/newFR/', views.fr_new, name='newFR'),
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)