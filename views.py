from django.shortcuts import render, redirect
from django.views import generic

from .models import fr, ds
from .forms import frForm
# Create your views here.

class indexView(generic.ListView):
    template_name = "efm/overview.html"
    context_object_name = "topLvlFr"
    model = fr
    
        
class efmTreeView(generic.DetailView):
    model = fr
    template_name = 'efm/efmView.html' 
    
class frDetail(generic.DetailView):
    model = fr
    template_name = "efm/frDetail.html"    
    
class dsDetail(generic.DetailView):
    model = ds
    template_name = "efm/dsDetail.html"
    
def fr_new(request, pk):
    if request.method == "POST":
        form = frForm(request.POST)
        if form.is_valid():
            newFR = form.save(commit=False)
            parentID = int(request.POST.get("rf", ""))
            newFR.rf = ds.objects.get(pk=parentID)
            newFR.save()
            return redirect('EFM:treeView', pk= parentID)
    else:
        form = frForm()
        parent = ds.objects.get(pk=pk)
    return render(request, 'efm/newFR.html', {'form': form, 'parent': parent,})
